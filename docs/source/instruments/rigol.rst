Rigol
=====

hardware_control.instruments.rigol.Rigol_DP832 module
-----------------------------------------------------

.. automodule:: hardware_control.instruments.rigol.Rigol_DP832
   :members:
   :undoc-members:
   :show-inheritance:

hardware_control.instruments.rigol.Rigol_DS1000Z module
-------------------------------------------------------

.. automodule:: hardware_control.instruments.rigol.Rigol_DS1000Z
   :members:
   :undoc-members:
   :show-inheritance:

