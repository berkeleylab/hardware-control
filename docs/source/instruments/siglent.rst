Siglent
=======

hardware_control.instruments..siglent.Siglent_SDG module
--------------------------------------------------------

.. automodule:: hardware_control.instruments.siglent.Siglent_SDG
   :members:
   :undoc-members:
   :show-inheritance:
