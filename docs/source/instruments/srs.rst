Stanford Research Instruments
=============================

hardware_control.instruments.srs.SRS_DG535 module
-------------------------------------------------

.. automodule:: hardware_control.instruments.srs.SRS_DG535
   :members:
   :undoc-members:
   :show-inheritance:

hardware_control.instruments.srs.SRS_CIS200 module
--------------------------------------------------

.. automodule:: hardware_control.instruments.srs.SRS_CIS200
   :members:
   :undoc-members:
   :show-inheritance:
