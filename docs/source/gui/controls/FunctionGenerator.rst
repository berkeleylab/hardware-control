Function Generators
===================

hardware_control.gui.controls.FunctionGenerator module
------------------------------------------------------

.. automodule:: hardware_control.gui.controls.FunctionGenerator
   :members:
   :undoc-members:
   :show-inheritance:
