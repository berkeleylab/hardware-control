Oscilloscopes
=============

hardware_control.gui.controls.Oscilloscope module
-------------------------------------------------

.. automodule:: hardware_control.gui.controls.Oscilloscope
   :members:
   :undoc-members:
   :show-inheritance:
