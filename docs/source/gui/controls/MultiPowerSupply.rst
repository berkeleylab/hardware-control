Multi-Channel Power Supplies
============================

hardware_control.gui.controls.MultiPowerSupply module
-----------------------------------------------------

.. automodule:: hardware_control.gui.controls.MultiPowerSupply
   :members:
   :undoc-members:
   :show-inheritance:
