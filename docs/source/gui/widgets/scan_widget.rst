Scan Widget
===========

hardware_control.gui.widgets.scan_widget module
-----------------------------------------------

.. automodule:: hardware_control.gui.widgets.scan_widget
   :members:
   :undoc-members:
   :show-inheritance:
