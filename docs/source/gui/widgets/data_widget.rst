Data Widget
===========


hardware_control.gui.widgets.data_widget module
-----------------------------------------------

.. automodule:: hardware_control.gui.widgets.data_widget
   :members:
   :undoc-members:
   :show-inheritance:
